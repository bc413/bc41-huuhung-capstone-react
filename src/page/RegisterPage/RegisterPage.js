import { Button, Form, Input, message } from "antd";
import { NavLink, useNavigate } from "react-router-dom";
import { UserSrv } from "../../service/useService";
import { useDispatch } from "react-redux";
import { USER_LOGIN } from "../../redux/constant/constant";
import { locaiStoage } from "../../service/locialService";

const formItemLayout = {
  labelCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 8,
    },
  },
  wrapperCol: {
    xs: {
      span: 24,
    },
    sm: {
      span: 16,
    },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};
const RegisterPage = () => {
  let navlink = useNavigate();
  let dispatch = useDispatch();
  const [form] = Form.useForm();
  const onFinish = (values) => {
    console.log("Received values of form: ", values);
    UserSrv.userRegister(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng ký thành công!");
        navlink("/");
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
        locaiStoage.set(res.data.content);
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng ký thất bại!");
      });
  };

  return (
    <div
      className="flex flex-row items-center justify-center h-screen "
      style={{ backgroundColor: "#a2d2ff", padding: "0 16px" }}
    >
      <div
        style={{
          backgroundColor: "#ffc8dd",
          padding: "48px",
          borderRadius: "20px",
          width: "600px",
        }}
      >
        <div
          style={{
            marginBottom: "30px",
            fontWeight: "600",
            fontSize: "30px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <h2>Đăng Ký</h2>
        </div>
        <Form
          {...formItemLayout}
          form={form}
          name="register"
          onFinish={onFinish}
          initialValues={{
            residence: ["zhejiang", "hangzhou", "xihu"],
            prefix: "86",
          }}
          style={{
            maxWidth: 600,
          }}
          scrollToFirstError
        >
          <Form.Item
            name="taiKhoan"
            label="Tài Khoản"
            rules={[
              {
                type: "taiKhoan",
                message: "The input is not valid",
              },
              {
                required: true,
                message: "Hãy nhập tài khoản",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="hoTen"
            label="Họ tên"
            rules={[
              {
                type: "hoTen",
                message: "The input is not valid",
              },
              {
                required: true,
                message: "Hãy nhập họ tên",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            name="email"
            label="E-mail"
            rules={[
              {
                type: "email",
                message: "Hãy nhập email",
              },
              {
                required: true,
                message: "Please input your E-mail!",
              },
            ]}
          >
            <Input />
          </Form.Item>

          <Form.Item
            name="matKhau"
            label="Password"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
            hasFeedback
          >
            <Input.Password />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Confirm Password"
            dependencies={["password"]}
            hasFeedback
            rules={[
              {
                required: true,
                message: "Please confirm your password!",
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue("matKhau") === value) {
                    return Promise.resolve();
                  }
                  return Promise.reject(
                    new Error(
                      "The two passwords that you entered do not match!"
                    )
                  );
                },
              }),
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="soDt"
            label="Số điện thoại"
            rules={[
              {
                type: "taiKhoan",
                message: "The input is not valid",
              },
              {
                required: true,
                message: "Hãy nhập số điện thoại",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item {...tailFormItemLayout}>
            <div className="flex justify-between">
              <Button className="bg-green-600" type="primary" htmlType="submit">
                Đăng ký
              </Button>
              <NavLink to="/login">
                <Button className="bg-red-500" type="primary" htmlType="submit">
                  Đăng nhập
                </Button>
              </NavLink>
            </div>
          </Form.Item>
        </Form>
      </div>
    </div>
  );
};
export default RegisterPage;
