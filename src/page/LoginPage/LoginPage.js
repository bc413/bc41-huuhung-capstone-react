import { Button, Checkbox, Form, Input, message } from "antd";
import { UserSrv } from "../../service/useService";
import { useDispatch } from "react-redux";
import { USER_LOGIN } from "../../redux/constant/constant";
import { NavLink, useNavigate } from "react-router-dom";
import { locaiStoage } from "../../service/locialService";

const LoginPage = () => {
  let dispatch = useDispatch();
  let navLink = useNavigate();
  const onFinish = (values) => {
    console.log("Success:", values);

    UserSrv.userLogin(values)
      .then((res) => {
        console.log(res);
        dispatch({
          type: USER_LOGIN,
          payload: res.data.content,
        });
        locaiStoage.set(res.data.content);
        message.success("Đăng nhập thành công!");
        navLink("/");
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng nhập thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  return (
    <div
      className="h-screen flex justify-center items-center"
      style={{ backgroundColor: "#a2d2ff" }}
    >
      <div
        style={{
          padding: "48px",
          backgroundColor: "#ffc8dd",
          borderRadius: "12px",
        }}
      >
        <div
          style={{
            marginBottom: "30px",
            fontWeight: "600",
            fontSize: "30px",
            display: "flex",
            justifyContent: "center",
          }}
        >
          <h2>Đăng Nhập</h2>
        </div>
        <Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 16,
          }}
          style={{
            maxWidth: 600,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: "Please input your username!",
              },
            ]}
          >
            <Input />
          </Form.Item>
          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: "Please input your password!",
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
          <Form.Item
            name="remember"
            valuePropName="checked"
            wrapperCol={{
              offset: 8,
              span: 16,
            }}
          >
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <div className="flex justify-between">
            <Button
              style={{ backgroundColor: "#4096ff" }}
              type="primary"
              htmlType="submit"
            >
              Đăng Nhập
            </Button>
            <NavLink to="/register">
              <Button style={{ backgroundColor: "red" }} type="primary">
                Đăng Ký
              </Button>
            </NavLink>
          </div>
        </Form>
      </div>
    </div>
  );
};
export default LoginPage;
