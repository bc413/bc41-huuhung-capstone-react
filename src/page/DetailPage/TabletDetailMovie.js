import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { dataMovie } from "../../service/movieService";
import { Progress, Space } from "antd";

export default function TabletDetailMovie() {
  let params = useParams();
  const [detaiMovie, setDetaiMovie] = useState([]);
  useEffect(() => {
    dataMovie
      .detailMovie(params.id)
      .then((res) => {
        // console.log(res);
        setDetaiMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(detaiMovie);
  let renderDetailMovie = () => {
    return (
      <div className=" detail flex-wrap ">
        <div className="image" style={{ maxWidth: "50%", flex: "0 0 50%" }}>
          <img
            style={{ height: "100%", Width: "100%", objectFit: "cover" }}
            src={detaiMovie.hinhAnh}
            alt=""
          />
        </div>
        <div className="text" style={{ maxWidth: "50%", flex: "0 0 50%" }}>
          <h2>{detaiMovie.tenPhim}</h2>
          <p className="my-7">
            <strong>Mô Tả: </strong>
            {detaiMovie.moTa}
          </p>
          <div className="my-7">
            <h4>
              <strong>Ngày giờ khởi chiếu</strong>
            </h4>
            <p>{detaiMovie.ngayKhoiChieu}</p>
          </div>
        </div>
        <div className="danhGia ml-0 mt-3">
          <div className="text-center">
            <h4 className=" mb-2">
              <strong>Đánh giá</strong>
            </h4>
            <Space wrap>
              <Progress
                type="circle"
                percent={detaiMovie.danhGia}
                format={(percent) => `${percent} Điểm`}
              />
            </Space>
          </div>
        </div>
      </div>
    );
  };
  return (
    <div style={{ backgroundColor: "#bde0fe", height: "100vh" }}>
      <div className="container">{renderDetailMovie()}</div>
    </div>
  );
}
