import React from "react";
import { Desktop, Mobile, Tablet } from "../../layout/reponsive/reponsive";
import DesktopDetailMovie from "./DesktopDetailMovie";
import TabletDetailMovie from "./TabletDetailMovie";
import MobileDetailMovie from "./MobileDetailMovie";

export default function DetailPage() {
  return (
    <div>
      <Desktop>
        <DesktopDetailMovie />
      </Desktop>
      <Tablet>
        <TabletDetailMovie />
      </Tablet>
      <Mobile>
        <MobileDetailMovie />
      </Mobile>
    </div>
  );
}
