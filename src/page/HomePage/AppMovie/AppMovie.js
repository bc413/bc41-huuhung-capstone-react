import React from "react";
import { Desktop, Mobile, Tablet } from "../../../layout/reponsive/reponsive";
import DesktopAppMovie from "./DesktopAppMovie";
import TabletAppMovie from "./TabletAppMovie";
import MobileAppMovie from "./MobileAppMovie";

export default function AppMovie() {
  return (
    <div>
      <Desktop>
        <DesktopAppMovie />
      </Desktop>
      <Tablet>
        <TabletAppMovie />
      </Tablet>
      <Mobile>
        <MobileAppMovie />
      </Mobile>
    </div>
  );
}
