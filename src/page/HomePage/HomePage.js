import React from "react";
import Banner from "../../Components/Banner/Banner";
import ListMovie from "./ListMovie/ListMovie";
import TabsMovie from "./TabsMovie/TabsMovie";
import AppMovie from "./AppMovie/AppMovie";

export default function HomePage() {
  return (
    <div>
      <Banner />
      <ListMovie />
      <TabsMovie />
      <AppMovie />
    </div>
  );
}
