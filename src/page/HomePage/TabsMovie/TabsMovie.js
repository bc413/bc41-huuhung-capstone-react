import React from "react";
import { Desktop, Tablet } from "../../../layout/reponsive/reponsive";
import DesktopTabsMovie from "./DesktopTabsMovie";
import TableTabsMovie from "./TableTabsMovie";
export default function TabsMovie() {
  return (
    <div>
      <Desktop>
        <DesktopTabsMovie />
      </Desktop>
      <Tablet>
        <TableTabsMovie />
      </Tablet>
    </div>
  );
}
