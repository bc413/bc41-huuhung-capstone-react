import { render } from "@testing-library/react";
import moment from "moment/moment";
import React from "react";

export default function NgayChieu({ item }) {
  console.log(item);
  let renderLichChieu = () => {
    return item.danhSachPhim.map((lichChieu, index) => {
      return (
        <div
          key={index}
          style={{
            display: "flex",
            margin: "30px 0",
          }}
        >
          <div className="mr-5">
            <img
              style={{
                width: "100px",
                height: "126px",
                objectFit: "cover",
                borderRadius: "10px",
              }}
              src={lichChieu.hinhAnh}
              alt=""
            />
          </div>
          <div>
            <h2
              style={{
                fontSize: "17px",
                fontWeight: "bold",
                marginBottom: "10px",
                color: "red",
              }}
            >
              {lichChieu.tenPhim}
            </h2>
            <div className="grid gap-5 grid-cols-2 ">
              {lichChieu.lstLichChieuTheoPhim.slice(0, 4).map((item, index) => {
                return (
                  <div
                    key={index}
                    className="rounded p-2 border border-indigo-600 text-black"
                  >
                    {moment(item.ngayChieuGioChieu).format(
                      "DD-mm-yyyy - hh:mm"
                    )}
                  </div>
                );
              })}
            </div>
          </div>
        </div>
      );
    });
  };
  return (
    <div style={{ overflowY: "scroll", height: "500px" }}>
      {renderLichChieu()}
    </div>
  );
}
