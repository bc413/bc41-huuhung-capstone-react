import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { dataMovie } from "../../../service/movieService";
import NgayChieu from "./NgayChieu";

export default function TableTabsMovie() {
  const onChange = (key) => {
    console.log(key);
  };
  const items = [
    {
      key: "1",
      label: `Tab 1`,
      children: `Content of Tab Pane 1`,
    },
  ];
  const [dtaHeThongRap, setdtaHeThongRap] = useState([]);
  useEffect(() => {
    dataMovie
      .heThongRap()
      .then((res) => {
        console.log(res);
        setdtaHeThongRap(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  console.log(dtaHeThongRap);
  let renderHeThongRap = () => {
    return dtaHeThongRap.map((item, index) => {
      return {
        key: item.maHeThongRap,
        label: <img style={{ width: "50px" }} src={`${item.logo}`} alt="" />,
        children: (
          <Tabs
            style={{ height: "500px" }}
            tabPosition={"left"}
            defaultActiveKey="1"
            items={item.lstCumRap.map((item, index) => {
              return {
                key: index,
                label: (
                  <div
                    style={{
                      width: "300px",
                      textAlign: "left",
                    }}
                  >
                    <h2>{item.tenCumRap}</h2>
                    <p
                      style={{
                        whiteSpace: "nowrap",
                        width: "300px",
                        overflow: "hidden",
                        textOverflow: "ellipsis",
                      }}
                    >
                      {item.diaChi}
                    </p>
                  </div>
                ),
                children: <NgayChieu item={item} />,
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };
  return (
    <div className="container hidden">
      <Tabs
        style={{ height: "500px" }}
        tabPosition={"left"}
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
