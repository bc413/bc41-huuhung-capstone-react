import React from "react";
import { Desktop, Mobile, Tablet } from "../../../layout/reponsive/reponsive";
import DesktopListMovie from "./DesktopListMovie";

import MobileListMovie from "./MobileListMovie";
import TabletListMovie from "./TabletListMovie";

export default function ListMovie() {
  return (
    <div>
      <Desktop>
        <DesktopListMovie />
      </Desktop>
      <Tablet>
        <TabletListMovie />
      </Tablet>
      <Mobile>
        <MobileListMovie />
      </Mobile>
    </div>
  );
}
