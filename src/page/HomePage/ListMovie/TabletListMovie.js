import React, { useEffect, useState } from "react";
import { dataMovie } from "../../../service/movieService";
import { Button, Card } from "antd";
import { NavLink } from "react-router-dom";
const { Meta } = Card;

export default function TabletListMovie() {
  const [listMovie, setListMovie] = useState([]);
  useEffect(() => {
    dataMovie
      .getListMovie()
      .then((res) => {
        setListMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderListMovie = () => {
    return listMovie.map((item, index) => {
      return (
        <Card
          key={index}
          hoverable
          style={{
            width: "210px",
            backgroundColor: "#bde0fe",
            textAlign: "center",
          }}
          cover={
            <img
              style={{
                height: "300px",
                objectFit: "cover",
                objectPosition: "top",
              }}
              alt="example"
              src={item.hinhAnh}
            />
          }
        >
          <Meta title={item.tenPhim} />
          <NavLink to={`/detail/${item.maPhim}`}>
            <Button className="mt-4" style={{ backgroundColor: "#ffc8dd" }}>
              Xem chi tiết
            </Button>
          </NavLink>
        </Card>
      );
    });
  };
  return (
    <div className=" py-12">
      <h2
        className="container text-center text-5xl font-bold"
        style={{ marginBottom: "30px", color: "#a2d2ff" }}
      >
        Danh Sách Phim
      </h2>
      <div className="container grid  grid-rows-3 grid-flow-col gap-4">
        {renderListMovie()}
      </div>
    </div>
  );
}
