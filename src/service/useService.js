import axios from "axios";
import { USE_PAGE, tokenHocVien } from "./config";

export const UserSrv = {
  userLogin: (values) => {
    return axios({
      url: `${USE_PAGE}/api/QuanLyNguoiDung/DangNhap`,
      method: "POST",
      data: values,
      headers: tokenHocVien(),
    });
  },
  userRegister: (values) => {
    return axios({
      url: `${USE_PAGE}/api/QuanLyNguoiDung/DangKy`,
      method: "POST",
      data: values,
      headers: tokenHocVien(),
    });
  },
};
