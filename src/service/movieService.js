import { https } from "./config";

export const dataMovie = {
  bannerMovie: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachBanner");
  },
  getListMovie: () => {
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP04");
  },
  detailMovie: (maPhim) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },
  heThongRap: () => {
    return https.get(
      "/api/QuanLyRap/LayThongTinLichChieuHeThongRap?maNhom=GP01"
    );
  },
};
