import axios from "axios";

export const USE_PAGE = "https://movienew.cybersoft.edu.vn";

const Token =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0ZW5Mb3AiOiJCb290Y2FtcCA0MSIsIkhldEhhblN0cmluZyI6IjEyLzA5LzIwMjMiLCJIZXRIYW5UaW1lIjoiMTY5NDQ3NjgwMDAwMCIsIm5iZiI6MTY2NTI0ODQwMCwiZXhwIjoxNjk0NjI0NDAwfQ.SUELcPShU58ZkNS3CbFDhM02KMzll9j00ndjVSaiJ8Q";

export const tokenHocVien = () => ({
  TokenCybersoft: Token,
});

export const https = axios.create({
  baseURL: USE_PAGE,
  headers: tokenHocVien(),
});
