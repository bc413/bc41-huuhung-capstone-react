let USER = "USER";

export const locaiStoage = {
  set: (values) => {
    let dataString = JSON.stringify(values);
    localStorage.setItem(USER, dataString);
  },
  get: () => {
    const getUSer = localStorage.getItem(USER);
    return JSON.parse(getUSer);
  },
  remove: () => {
    localStorage.removeItem(USER);
  },
};
