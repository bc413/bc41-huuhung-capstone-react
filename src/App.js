import logo from "./logo.svg";
import "./App.css";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { userRoute } from "./router/userRoute";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Routes>
          {userRoute.map((item, index) => {
            return (
              <Route key={index} path={item.url} element={item.components} />
            );
          })}
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;
