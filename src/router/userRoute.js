import Layout from "../layout/Layout";
import DetailPage from "../page/DetailPage/DetailPage";
import HomePage from "../page/HomePage/HomePage";
import LoginPage from "../page/LoginPage/LoginPage";
import NotfoundPage from "../page/NotfoundPage/NotfoundPage";
import RegisterPage from "../page/RegisterPage/RegisterPage";

export const userRoute = [
  {
    url: "/",
    components: <Layout Components={HomePage} />,
  },
  {
    url: "/login",
    components: <LoginPage />,
  },
  {
    url: "/register",
    components: <RegisterPage />,
  },
  {
    url: "/detail/:id",
    components: <Layout Components={DetailPage} />,
  },
  {
    url: "*",
    components: <Layout Components={NotfoundPage} />,
  },
];
