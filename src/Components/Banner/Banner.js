import { Carousel } from "antd";
import { useEffect, useState } from "react";
import { dataMovie } from "../../service/movieService";
const contentStyle = {
  height: "600px",
  width: "100%",
  color: "#fff",
  lineHeight: "160px",
  textAlign: "center",
  background: "#364d79",
};

const Banner = () => {
  const [bannerMovie, setBannerMovie] = useState([]);
  useEffect(() => {
    dataMovie
      .bannerMovie()
      .then((res) => {
        // console.log(res);
        setBannerMovie(res.data.content);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  let renderBanner = () => {
    return bannerMovie.map((item, index) => {
      return (
        <div key={index}>
          <h3 style={contentStyle}>
            <img
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
              src={item.hinhAnh}
              alt=""
            />
          </h3>
        </div>
      );
    });
  };
  return <Carousel autoplay>{renderBanner()}</Carousel>;
};
export default Banner;
