import React from "react";
import { Desktop, Mobile, Tablet } from "../../layout/reponsive/reponsive";
import DesktopFooter from "./DesktopFooter";
import TabletFooter from "./TabletFooter";
import MobileFooter from "./MobileFooter";

export default function Footer() {
  return (
    <div>
      <Desktop>
        <DesktopFooter />
      </Desktop>
      <Tablet>
        <TabletFooter />
      </Tablet>
      <Mobile>
        <MobileFooter />
      </Mobile>
    </div>
  );
}
