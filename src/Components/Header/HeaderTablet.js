import React from "react";
import LogoHeader from "./LogoHeader";
import MenuHeader from "./MenuHeader";

export default function HeaderDesktop() {
  return (
    <div className="header py-3">
      <div className="flex container items-center">
        <div className="container flex items-center justify-between">
          <img style={{ width: "200px" }} src="./logo.png" alt="" />
        </div>
        <MenuHeader />
      </div>
    </div>
  );
}
