import React from "react";
import LogoHeader from "./LogoHeader";
import MenuHeader from "./MenuHeader";

export default function HeaderDesktop() {
  return (
    <div className="header py-3">
      <div className="container flex items-center justify-between">
        <LogoHeader />
        <MenuHeader />
      </div>
    </div>
  );
}
