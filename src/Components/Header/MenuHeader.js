import { Button } from "antd";
import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { locaiStoage } from "../../service/locialService";

export default function MenuHeader() {
  let dataUser = useSelector((state) => {
    return state.userReducer.dataUser;
  });
  let handleLogout = () => {
    locaiStoage.remove();
    window.location.reload();
  };
  let renderUser = () => {
    if (dataUser) {
      return (
        <div className=" flex items-center">
          <p className="md:text-[16px] md:w-[100%] text-[12px]">
            {dataUser.hoTen}
          </p>
          <Button
            onClick={handleLogout}
            className="ml-3 md:text-[14px] text-[10px]"
          >
            Đăng Xuất
          </Button>
        </div>
      );
    } else {
      return (
        <div className="flex">
          <NavLink to="/login">
            <Button className="mr-3">Đăng nhập</Button>
          </NavLink>
          <NavLink to="/register">
            <Button>Đăng ký</Button>
          </NavLink>
        </div>
      );
    }
  };
  return (
    <div>
      <div>{renderUser()}</div>
    </div>
  );
}
