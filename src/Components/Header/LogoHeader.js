import React from "react";
import { NavLink } from "react-router-dom";

export default function LogoHeader() {
  return (
    <div className="flex items-center">
      <NavLink to="/">
        <img style={{ width: "200px" }} src="./logo.png" alt="" />
      </NavLink>
      <div className="ml-5">
        <ul className="flex menu">
          <NavLink to="/">
            <li>Trang Chủ</li>
          </NavLink>
          <li>Liện Hệ</li>
          <li>Tin Tức</li>
          <li>Ứng Dụng</li>
        </ul>
      </div>
    </div>
  );
}
