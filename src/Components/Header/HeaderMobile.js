import React from "react";
import LogoHeader from "./LogoHeader";
import { NavLink } from "react-router-dom";
import MenuHeader from "./MenuHeader";

export default function HeaderMobile() {
  return (
    <div className=" flex py-3 justify-around">
      <NavLink to="/">
        <img style={{ width: "100px" }} src="./logo.png" alt="" />
      </NavLink>
      <MenuHeader />
    </div>
  );
}
