import { locaiStoage } from "../../service/locialService";
import { USER_LOGIN } from "../constant/constant";

const initialState = {
  dataUser: locaiStoage.get(),
};

let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN: {
      return { ...state, dataUser: payload };
    }

    default:
      return state;
  }
};
export default userReducer;
