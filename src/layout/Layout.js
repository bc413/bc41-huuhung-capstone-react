import React from "react";
import Header from "../Components/Header/Header";
import Footer from "../Components/Footer/Footer";

export default function Layout({ Components }) {
  return (
    <div>
      <Header />
      <Components />
      <Footer />
    </div>
  );
}
